## 1- INTRODUCTION

This project is the source code of the website `1bet.fr`. It is done with the MVC Framework Django and relies on
a PostgreSQL database.

The goal of this website is to aggregate news, scores, rankings and other data about sports, and to propose an
interface of free sportive bets between friends.

The front is done with Bootstrap Framework, JQuery and the compiled language SCSS.


## 2- DEPLOYMENT

To deploy the project, first create a virtualenv :

    python3.7 -m venv venv
    source venv/bin/activate
    pip install -r doc/requirements.txt
    
To create database schema, use manage.py :

    cd onebet/
    python manage.py migrate
    
To show current migrations :
    
    python manage.py showmigrations
    
To reset migrations :

    python manage.py migrate mainapp zero
    
To make migration files :

    python manage.py makemigrations
    
To compile SCSS files :
    
    python manage.py compilescss
   
To run server on port 8000 :

    python manage.py runserver
