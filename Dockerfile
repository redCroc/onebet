FROM python:3.7-buster

WORKDIR /usr/src/app
COPY doc/requirements.txt .
RUN pip install -r requirements.txt

COPY onebet/mainapp mainapp
COPY onebet/onebet onebet
COPY onebet/scripts scripts
COPY onebet/manage.py manage.py
RUN rm -f onebet/settings_local.py
EXPOSE 8000

RUN chown -R www-data .
USER www-data
