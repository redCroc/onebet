class Session {
    static seenBuildingModal() {
        $.ajax({
            url: "/api/user/session",
            method: "POST",
            data: JSON.stringify({seenBuildingModal: true}),
            contentType: "application/json"
        })
    }
}