class Match {
    constructor(id, league, team_home, team_away, player_home, player_away, round, mday, start_date, home_score, away_score, sets_score) {
        this.id = id;
        this.league = league;
        this.team_home = team_home;
        this.team_away = team_away;
        this.player_home = player_home;
        this.player_away = player_away;
        this.round = round;
        this.mday = mday;
        this.start_date = start_date;
        this.home_score = home_score;
        this.away_score = away_score;
        this.sets_score = sets_score;
    }

    display(titleLeagues, titleRounds, titleMdays, titleDays, lastLeagueId, lastRound, lastMatchDay, lastDay, refElement) {
        let matchId = "match-" + this.id;
        let matchHref = "/match/" + this.team_home.clean_name + "/" + this.team_away.clean_name;
        let matchElement = null;
        let titleElement = null;

        if (titleLeagues) {
            if (this.league.id !== lastLeagueId) {
                $("<div></div>").addClass("text-center mt-4 px-3 py-1 text-dark font-weight-bold border-bottom border-dark").append(
                    $("<div></div>").append(
                        $("<img>").attr("src", "/static/img/league/" + this.league.images.H50).attr("alt", this.league.name)
                    ),
                    $("<div></div>").text(this.league.name)
                ).insertBefore(refElement);
            }
        }

        if (titleRounds) {
            if (this.round !== null && this.round !== lastRound) {
                $("<div></div>").addClass("text-left mt-4 px-3 py-1 bg-gray text-light font-weight-bold text-capitalize").text(
                    this.round
                ).insertBefore(refElement);
            }
        }

        if (titleMdays) {
            if (this.mday !== null && this.mday !== lastMatchDay) {
                $("<div></div>").addClass("text-left mt-4 px-3 py-1 bg-gray text-light font-weight-bold text-capitalize").text(
                    "Journée " + this.mday
                ).insertBefore(refElement);
            }
        }

        if (titleDays) {
            if (moment(this.start_date).format("L") !== lastDay) {
                $("<div></div>").addClass("text-left mt-3 px-2 py-1 text-dark font-weight-bold border-bottom border-dark text-capitalize").text(
                    moment(this.start_date).format("dddd Do MMMM")
                ).insertBefore(refElement);
            }
        }

        $("<div></div>").addClass("row my-2 p-2").append(
            $("<div></div>").addClass("col-5 px-2 text-left text-nowrap text-truncate").append(
                $("<img>").addClass("px-1").attr("src", "/static/img/team/" + this.team_home.images.H30),
                $("<span></span>").addClass("px-1").text(this.team_home.name)
            ),
            $("<div></div>").addClass("col-2 px-2 text-center").append(
                $("<span></span>").addClass("badge badge-dark").text(moment(this.start_date).format("LT"))
            ),
            $("<div></div>").addClass("col-5 px-2 text-right text-nowrap text-truncate").append(
                $("<span></span>").addClass("px-1").text(this.team_away.name),
                $("<img>").addClass("px-1").attr("src", "/static/img/team/" + this.team_away.images.H30)
            )
        ).insertBefore(refElement);
    }

    static displayMatchList(sportId, leagueId, startDate, endDate, location) {
        Match.disableScroll();
        $.ajax({
            url: "/api/match/list",
            method: "GET",
            data: {
                sportId, sportId,
                leagueId: leagueId,
                start: startDate ? startDate.format("X") : null,
                end: endDate ? endDate.format("X") : null
            },
            dataType: "json",
            success: function (data) {
                let currentRound = null;
                let currentMatchDay = null;
                let currentDay = null;
                let currentLeagueId = null;
                let titleLeagues = leagueId === null;
                let titleRounds = leagueId !== null;
                let titleMdays = leagueId !== null;
                let titleDays = leagueId !== null;
                let refElement = location === "top" ? $("#match > :not(#matchPrevious)").first() : $("#matchNext");

                if (leagueId === null) {
                    $("<div></div>").addClass("text-center mt-5 px-3 py-1 bg-gray text-light font-weight-bold text-capitalize").text(
                        moment(startDate).format("dddd Do MMMM")
                    ).insertBefore(refElement);
                }

                if (data.matches.length > 0) {
                    $.each(data.matches, function (index, m) {
                        new Match(
                            m.id, m.league, m.team_home, m.team_away, m.player_home, m.player_away, m.round, m.mday,
                            m.start_date, m.home_score, m.away_score, m.sets_score
                        ).display(
                            titleLeagues, titleRounds, titleMdays, titleDays,
                            currentLeagueId, currentRound, currentMatchDay, currentDay, refElement
                        );
                        currentRound = m.round;
                        currentMatchDay = m.mday;
                        currentDay = moment(m.start_date).format("L");
                        currentLeagueId = m.league.id;
                    });
                } else if (leagueId === null) {
                    $("<div></div>").addClass("text-center my-2 p-2 text-muted font-weight-bold").text(
                        "Aucun match sur cette période !"
                    ).insertBefore(refElement);
                }
                Match.enableScroll();
                scrollLock = false;
            },
            error: function (data) {
                console.log(data);
            }
        })
    }

    static disableScroll() {
        $("#matchPrevious").attr("disabled", "");
        $("#matchPrevious .labelContainer").addClass("spinner-grow spinner-grow-sm");
        $("#matchPrevious .label").addClass("sr-only");
        $("#matchNext").attr("disabled", "");
        $("#matchNext .labelContainer").addClass("spinner-grow spinner-grow-sm");
        $("#matchNext .label").addClass("sr-only");
    }

    static enableScroll() {
        $("#matchPrevious").removeAttr("disabled");
        $("#matchPrevious .labelContainer").removeClass("spinner-grow spinner-grow-sm");
        $("#matchPrevious .label").removeClass("sr-only");
        $("#matchNext").removeAttr("disabled");
        $("#matchNext .labelContainer").removeClass("spinner-grow spinner-grow-sm");
        $("#matchNext .label").removeClass("sr-only");
    }
}
