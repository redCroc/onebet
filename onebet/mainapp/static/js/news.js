class News {
    constructor(id, title, clean_title, pub_date, description, link, image, source, tags, redirect) {
        this.id = id;
        this.title = title;
        this.clean_title = clean_title;
        this.pub_date = pub_date;
        this.description = description;
        this.link = link;
        this.image = image;
        this.source = source;
        this.tags = tags;
        this.redirect = redirect;
    }

    display() {
        let newsId = "news-"+this.id;
        let newsHref = this.redirect ? this.redirect : "/news/" + this.clean_title;
        let newsImage = this.image ? this.image : "/static/img/source/" + this.source.big_image_name;

        $("#news").append(
            $("<div></div>").addClass("text-center").append(
                $("<div></div>").addClass("card w-100 d-inline-block bg-light mb-3 border-0 rounded-0").attr("id", newsId).append(
                    $("<a></a>").attr("href", newsHref).attr("target", this.redirect ? "_blank" : "_self").addClass("text-decoration-none").append(
                        $("<img>").addClass("card-img-top rounded-0").attr("src", newsImage).attr("alt", "src")
                    ),
                    $("<div></div>").addClass("card-body").append(
                        $("<a></a>").attr("href", newsHref).attr("target", this.redirect ? "_blank" : "_self").addClass("text-decoration-none").append(
                            $("<h5></h5>").addClass("card-title p-0 m-2 text-dark").text(this.title)
                        ),
                        $("<div></div>").addClass("text-right p-0 m-2 text-muted").text(moment(this.pub_date).fromNow()),
                        $("<div></div>").addClass("tags overflow-hidden p-0 m-2")
                    ),
                )
            )
        );
        $.each(this.tags.slice(0, 4), function (index, tag) {
            $("#" + newsId).find(".tags").append(
                $("<button></button>").attr("type", "button").addClass("card-link btn btn-light border-primary px-2 py-1 my-1 mx-2").append(
                    $("<a></a>").attr("href", "/tag/" + tag[1]).addClass("text-primary small").append(
                        $("<i></i>").addClass("fa fa-tag pl-1").attr("aria-hidden", "true"),
                        $("<span></span>").addClass("pl-1").text(" " + tag[0])
                    )
                )
            )
        });
    }

    static displayNewsList(sportId, leagueId, search, tag, offsetId, quantity) {
        $.ajax({
            url: "/api/news/list",
            method: "GET",
            data: {
                sportId: sportId,
                leagueId: leagueId,
                search: search,
                tag: tag,
                offsetId: offsetId,
                quantity: quantity
            },
            dataType: "json",
            success: function (data) {
                if ('news' in data) {
                    $.each(data.news, function (index, n) {
                        new News(n.id, n.title, n.clean_title, n.pub_date, n.description, n.link, n.image, n.source, n.tags, n.redirect).display();
                    })
                }
                scrollLock = false;
            },
            error: function (data) {
                console.log(data);
            }
        })
    }
}
