import django.urls

import mainapp.views


urlpatterns = [
    django.urls.path('api/news/list', mainapp.views.api_news_list, name='news_list'),
    django.urls.path('api/match/list', mainapp.views.api_match_list, name='match_list'),
    django.urls.path('api/user/session', mainapp.views.api_user_session, name='user_session'),

    django.urls.path('news/<title>', mainapp.views.view_news, name='news'),

    django.urls.path('', mainapp.views.view_index, name='index'),
    django.urls.path('tag/<tag>', mainapp.views.view_index, name='index'),
    django.urls.path('search/<search>', mainapp.views.view_index, name='index'),
    django.urls.path('<sport>', mainapp.views.view_index, name='index'),
    django.urls.path('<sport>/<league>', mainapp.views.view_index, name='index'),
]
