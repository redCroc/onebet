from django.contrib import admin

import mainapp.models


admin.site.register(mainapp.models.Sport)
admin.site.register(mainapp.models.Country)
admin.site.register(mainapp.models.League)
admin.site.register(mainapp.models.Team)
admin.site.register(mainapp.models.Player)
admin.site.register(mainapp.models.Match)
admin.site.register(mainapp.models.Source)
admin.site.register(mainapp.models.News)
