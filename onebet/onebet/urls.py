from django.contrib import admin
import django.urls


urlpatterns = [
    django.urls.path('', django.urls.include('mainapp.urls')),

    django.urls.path('youwontfindmyadministratorpath-591/', admin.site.urls),
]
