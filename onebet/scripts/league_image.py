import shutil
import os

import mainapp.models


class LeagueImage:

    OLD_PATH = os.path.join(
        os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(__file__)))),
        'bob-prod', 'images', 'league'
    )
    NEW_PATH = os.path.join(
        os.path.dirname(os.path.dirname(__file__)),
        'mainapp', 'static', 'img', 'league'
    )

    @classmethod
    def update_images(cls):
        for league in mainapp.models.League.objects.all():
            league.images = dict()
            for key in '35', '50', '80':
                old_path = os.path.join(cls.OLD_PATH, f'h{key}-{league.id}.svg')
                new_path = os.path.join(cls.NEW_PATH, f'l{league.id}-h{key}.svg')
                shutil.copy2(old_path, new_path)
                league.images['H'+key] = os.path.basename(new_path) + '?t=' + str(int(os.path.getmtime(new_path)))
            league.save()
