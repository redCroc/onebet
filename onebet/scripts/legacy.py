import json

import mysql.connector
import django.db.utils
import redis

import onebet.settings
import mainapp.models


class Legacy:
    def __init__(self, host=onebet.settings.MYSQL_HOST, port=onebet.settings.MYSQL_PORT,
                 username=onebet.settings.MYSQL_USERNAME, password=onebet.settings.MYSQL_PASSWORD,
                 database=onebet.settings.MYSQL_DATABASE):
        self._host = host
        self._port = port
        self._username = username
        self._password = password
        self._database = database
        self._connection = None
        self._cursor = None

    def _open_cursor(self):
        if not self._connection:
            self._connection = mysql.connector.connect(
                host=self._host, port=self._port, user=self._username,
                password=self._password, db=self._database
            )
        if not self._cursor:
            self._cursor = self._connection.cursor()

    def _close_cursor(self):
        if self._cursor:
            self._cursor.close()
        if self._connection:
            self._connection.close()

    def _migrate_sports(self):
        print('[+] Starting migrate sports...')
        mainapp.models.Sport.objects.all().delete()
        self._cursor.execute("SELECT * FROM sports")
        for row in self._cursor.fetchall():
            points = list(map(int, row[4].split(',')))
            sport = mainapp.models.Sport(
                id=row[0],
                name=row[1],
                clean_name=mainapp.models.Utils.sanitize(row[1]),
                display_sets=bool(row[2]),
                points={'win': points[0], 'tie': points[1], 'loss': points[2]},
                enabled=bool(row[5])
            )
            sport.save()

    def _migrate_countries(self):
        print('[+] Starting migrate countries...')
        mainapp.models.Country.objects.all().delete()
        self._cursor.execute("SELECT * FROM countries")
        for row in self._cursor.fetchall():
            country = mainapp.models.Country(
                id=row[0],
                name=row[1],
                clean_name=mainapp.models.Utils.sanitize(row[1]),
                short_name=row[2],
                source_names=json.loads(row[3])
            )
            country.save()

    def _migrate_leagues(self):
        print('[+] Starting migrate leagues...')
        mainapp.models.League.objects.all().delete()
        self._cursor.execute("SELECT * FROM leagues WHERE id < 700")
        for row in self._cursor.fetchall():
            urls = dict()
            if row[17]:
                urls['schedule'] = row[15]
            if row[18]:
                urls['ranking'] = row[16]
            if row[19]:
                urls['tvschedule'] = row[17]
            tags = json.loads(row[27]) or []
            league = mainapp.models.League(
                id=row[0],
                sport=mainapp.models.Sport(id=row[1]),
                country=mainapp.models.Country(id=row[2]),
                name=row[3],
                clean_name=mainapp.models.Utils.sanitize(row[3]),
                degree=row[7],
                gender={'M': 1, 'F': 2}[row[8]],
                schedule_url=row[17] or None,
                ranking_url=row[18] or None,
                channel_url=row[19] or None,
                mdays=row[9] or None,
                current_mday=row[10] or None,
                matches_by_mday=row[11] or None,
                team_count=row[12] or 0,
                rounds=row[13].split(',') if row[13] else None,
                groups=row[14].split(',') if row[14] else None,
                points=json.loads(row[15]) if row[15] else None,
                promotions=json.loads(row[16]) if row[16] else None,
                images=json.loads(row[26]),
                tags=tags,
                clean_tags=[mainapp.models.Utils.sanitize(tag) for tag in tags],
                news_count=row[28],
                error=None,
                trace=None
            )
            league.save()

    def _migrate_sources(self):
        print('[+] Starting migrate news sources...')
        names = {
            'lequipe': "L'équipe",
            'eurosport': 'Eurosport',
            'fftt': 'FFTT',
            'foot-mercato': 'Foot Mercato'
        }
        mainapp.models.Source.objects.all().delete()
        self._cursor.execute("SELECT * FROM news_source")
        for row in self._cursor.fetchall():
            name = "Rugbyrama" if "rugbyrama.fr" in row[3] else names[row[2]]
            source = mainapp.models.Source(
                sport=mainapp.models.Sport(id=row[1]),
                name=name,
                clean_name=mainapp.models.Utils.sanitize(name),
                image_name=mainapp.models.Utils.sanitize(name) + '.png',
                big_image_name='big-' + mainapp.models.Utils.sanitize(name) + '.png',
                feed_url=row[3]
            )

            try:
                source.save()
            except django.db.utils.IntegrityError:
                pass

    def _migrate_agents(self):
        print('[+] Starting migrate user agents...')
        red = redis.Redis()
        self._cursor.execute("SELECT useragents FROM user_agents")
        agents = [row[0] for row in self._cursor.fetchall()]
        red.sadd('agents', *agents)

    def migrate(self):
        self._open_cursor()
        self._migrate_sports()
        self._migrate_countries()
        self._migrate_leagues()
        self._migrate_sources()
        self._migrate_agents()
        self._close_cursor()
